package com.example.rencio.animateimage;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import java.util.Timer;

public class MainActivity extends AppCompatActivity {
    ImageView img1,img2;
    int choice=0;
    public void animateImage1(View v){

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        img1= (ImageView) findViewById(R.id.img1);
        img2= (ImageView) findViewById(R.id.img2);
        if(choice%2==0){
            img2.animate().alpha(1f).setDuration(4000).rotationBy(36000);
            img1.animate().alpha(0f).setDuration(4000).rotationBy(360000);
            choice++;
        }
        else{
            img2.animate().alpha(0f).setDuration(4000).rotationBy(36000);
            img1.animate().alpha(1f).setDuration(4000).rotationBy(36000);
            choice++;
        }
        final Thread timerThread = new Thread(){
            public void run(){
                try{
                    sleep(5000);
                }
                catch(InterruptedException e){
                    e.printStackTrace();
                }
                finally{
                    Intent i= new Intent(MainActivity.this,Main2Activity.class);
                    startActivity(i);
                }
            }
        };
        timerThread.start();
    }

    @Override
    protected void onPause(){
        finish();
        super.onPause();
    }
}
